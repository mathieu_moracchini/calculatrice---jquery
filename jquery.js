let firstNb = '';
let operation = '';
let secondNb = '';

$(document).ready(function () {
    $('.calc').hide();
    setTimeout(function () {
        $('.calc').slideDown(); //effet
    }, 500);
    
    $('.less').click(function(){
        $('.calc').animate({
            width : '258px',
            height : '30px'
        });
        $('.calculator-body').addClass('hidden');
    });

    $('.cube').click(function(){
        $('.calc').animate({
            width : '258px',
            height : '514px'
        });
        $('.calculator-body').removeClass('hidden');
    });

    $('.arguments').click(function(event){
        let action = $(event.target).attr('data-id'); //target => permet de cibler un element lors dun click & attr permet de prendre l'attribut
        console.log(action);

        if (action === '+' || action === '-' || action === '*' || action === '/' || action === '%' || action === '^2') {
            operation = action;
        } else if (action === '=') {
            let result;

            let nb1 = parseFloat(firstNb);
            let nb2 = parseFloat(secondNb);

            if (operation === '+') {
                result = nb1 + nb2;
            } else if (operation === '-') {
                result = nb1 - nb2;
            } else if (operation === '*') {
                result = nb1 * nb2;
            } else if (operation === '/') {
                result = nb1 / nb2;
            } else if (operation === '%') {
                result = nb1 / 100;
            } else if (operation === '^2') {
                result = nb1 * nb1;
            }

            firstNb = result;
            secondNb = '';
            operation = '';
        } else if (action === 'C') {
            firstNb = '';
            secondNb = '';
            operation = '';
        } else {
            if (operation === '') {
                firstNb = firstNb + action;
            } else {
                secondNb = secondNb + action;
            }
        }

        $('#firstNb').html(firstNb);
        $('#operation').html(operation);
        $('#currentNb').html(secondNb);
    });
});